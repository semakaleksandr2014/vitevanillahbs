import { defineConfig } from "vite";
import sass from "sass";
import { resolve } from "path";
import handlebars from "vite-plugin-handlebars";
import imagemin from "vite-plugin-imagemin";

export default defineConfig({
  plugins: [
    handlebars({
      partialDirectory: __dirname + "/src/partials",
    }),
    imagemin({
      gifsicle: {},
      mozjpeg: {},
      optipng: {},
      webp: {},
    }),
  ],
  build: {
    polyfillModulePreload: false,
    rollupOptions: {
      input: {
        main: resolve(__dirname, "index.html"),
        career: resolve(__dirname, "career.html"),
      },
    },
  },
  css: {
    preprocessorOptions: {
      sass: {
        implementation: sass,
      },
    },
  },
});
